def nombre_entier(n: int) -> str:
    if n == 0 :
        return "0"
    resultult = "" 
    for i in range (0, n):
        resultult += "S"
    resultult += "0"
    return(resultult)

def S(n: str) -> str:
    n = "S" + n
    return n

def addition(a: str, b: str) -> str:
    if a == "0":
        return b

    if a[0] == "S" :
        return S(addition(a[1:], b))

def multiplication(a: str, b: str) -> str:
    if a == "0":
        return "0"
    if a[0] == "S":
        return addition(multiplication(a[1:], b), b) 

def facto_ite(n: int) -> int:
    if n == 0:
        return 1
    result = 1
    for i in range (2, n+1): 
        result = result * i
    return result

def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    return (n * facto_rec(n-1))

def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return ((fibo_rec(n-1)) + (fibo_rec(n-2)))

def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n == 2:
        return 1
    result = 0
    fibo1 = 1
    fibo2 = 1
    for i in range (3, n+1):
        result = fibo1 + fibo2
        fibo2 = fibo1
        fibo1 = result 
    return result

def golden_phi(n: int) -> int:
    return fibo_ite(n+1) / fibo_ite(n)

def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) -1

def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    elif n == 1:
        return a
    elif n < 0:
        return 1 / pow(a, -n)
    elif n % 2 == 0:
        return pow(a * a, n // 2)
    else:
        return a * pow(a, n - 1)
